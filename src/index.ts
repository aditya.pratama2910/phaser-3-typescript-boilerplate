import Phaser from "phaser";
import PreloadScene from "./scene/PreloadScene";

const config = {
  type: Phaser.AUTO,
  parent: "phaser-example",
  width: 800,
  height: 600,
  scene: [ PreloadScene ]
};

export default new Phaser.Game(config);
